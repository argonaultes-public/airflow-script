```bash
docker run -d --name airflowlocal -p 8181:8080 -v /home/gael/Projects/argonaultes/education/airflowdags:/opt/airflow/dags apache/airflow:latest standalone
```

```bash
docker run -d --name test-oracle-db -p 1521:1521 container-registry.oracle.com/database/express:21.3.0-xe
```

```bash
docker exec -it test-oracle-db sqlplus / as sysdba
```

```sql
alter session set container = XEPDB1;
create user tpsql identified by tpsql;
grant all privileges to tpsql;
```

```bash
docker exec -it test-oracle-db sqlplus tpsql/tpsql@XEPDB1
```

```sql
create table test_insert(id int, test_comment varchar2(255));
```

