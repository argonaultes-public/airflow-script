import datetime

from airflow import DAG
from airflow.operators.bash import BashOperator

with DAG(
        dag_id="crondag5",
        schedule="* * * * *",
        catchup=False,
        start_date=datetime.datetime(2024, 5, 27),
        is_paused_upon_creation=False):
    t1 = BashOperator(
        task_id="t1",
        bash_command="date"
    )

with DAG(
    dag_id="crondag30sec",
    schedule=datetime.timedelta(seconds=30),
    start_date=datetime.datetime(2024, 5, 27),
    is_paused_upon_creation=False,
    catchup=False):
    t1 = BashOperator(
        task_id="t130sec",
        bash_command="date"
        )