from airflow import DAG
from airflow.operators.empty import EmptyOperator


with DAG(dag_id="emptydag"):
    t1 = EmptyOperator(task_id="task1")
    t2 = EmptyOperator(task_id="task2")
    t3 = EmptyOperator(task_id="task3")
    t4 = EmptyOperator(task_id="task4")
    t5 = EmptyOperator(task_id="task5")
    [t1, t3] >> t2 >> [t4, t5]
