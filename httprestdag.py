from airflow import DAG
from airflow.providers.http.operators.http import HttpOperator

with DAG(dag_id="httprestdag"):
    t1 = HttpOperator(
        task_id="gethttpjson",
        http_conn_id="http_rest_profile",
        method="GET",
        endpoint="/lastprofile",
        log_response=True
    )