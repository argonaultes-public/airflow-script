from __future__ import annotations

from airflow.decorators import dag, task
from airflow.models.baseoperator import chain

@dag(schedule=None, is_paused_upon_creation=True)
def example_dag_annotation():

    @task.bash
    def task_bash_1() -> str:
        return "echo task bash 1"

    task_bash_1_res = task_bash_1()

    @task.bash
    def task_bash_2() -> str:
        return "echo task bash 2"

    task_bash_2_res = task_bash_2()

    chain(task_bash_1_res, task_bash_2_res)

example_dag_annotation()