from airflow import DAG
from airflow.operators.bash import BashOperator


with DAG(
    dag_id='onetime',
    schedule=None,
    is_paused_upon_creation=False):
    t1 = BashOperator(
        task_id='onetimetask',
        bash_command='echo "run one time task"'
    )