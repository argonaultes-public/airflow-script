from airflow import DAG
from airflow.providers.postgres.operators.postgres import PostgresOperator
from airflow.operators.python import PythonOperator


def process_response(task_instance):
    response = task_instance.xcom_pull(key="return_value", task_ids="t1")
    for item in response:
        print(item)

with DAG(dag_id="postgresdag"):
    t1 = PostgresOperator(
        task_id="t1",
        postgres_conn_id="test_postgres",
        sql="SELECT * FROM my_table;",
    )
    t2 = PythonOperator(
        task_id="t2",
        python_callable=process_response
    )
    t1 >> t2