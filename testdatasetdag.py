from airflow import DAG
from airflow.datasets import Dataset
from airflow.operators.bash import BashOperator
import datetime

sampledataset = Dataset("file://hello/boys")

with DAG(dag_id="origin_dag",start_date=datetime.datetime(2024,5,21),schedule=None):
    t1 = BashOperator(
        task_id="origin_t1",
        bash_command='echo t1',
        outlets=[sampledataset]
    )

with DAG(dag_id="triggered_1",schedule=[sampledataset],start_date=datetime.datetime(2024,5,21)):
    t1 = BashOperator(
        task_id="t2",
        bash_command="echo trop_fort"
    )
